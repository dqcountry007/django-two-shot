from django.urls import path

from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    ExpenseCategoryListView,
    ExpenseCategoryCreateView,
    AccountListView,
    AccountCreateView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipts_create"),
    path(
        "expense_categories/list/",
        ExpenseCategoryListView.as_view(),
        name="expense_category_list",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="expense_category_create",
    ),
    path("accounts/list/", AccountListView.as_view(), name="account_list"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="account_create"
    ),
]
